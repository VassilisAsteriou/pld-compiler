
#include <string>
#include <fnb.hpp>
#include <optimizer.hpp>

struct argstruct {
    std::string input_fname = "";
    std::string output_fname = "out.mixal";
    bool optimizations = false;
    bool verbose = false;
    bool version = false;
};

argstruct parseArgs(int argc, char *argv[]) {
    argc--; argv++;         // Shift executable name

    argstruct rv;
    while(argc > 0) {
        std::string s(*argv);
        if(s == "-o" || s == "--output") {
            argc--; argv++;
            if(argc) {
                rv.output_fname = std::string(*argv);
            }
            else {
                log::warn("Argument of output option missing. Using 'out.mix'");
                break;
            }
        } else if (s == "-O1") {
            rv.optimizations = true;
        } else if (s == "-v" || s == "--verbose") {
            rv.verbose = true;
        } else if (s == "--version") {
            rv.version = true;
        } else {
            rv.input_fname = s;
        }
        argc--; argv++;
    }

    return rv;
}

int main(int argc, char *argv[])
{

    auto args = parseArgs(argc, argv);

    if(args.version) {
        std::cout << "lang v0.3" << std::endl;
        exit(0);
    }

    try {
        auto ast = driver{args.input_fname}();

        if(args.verbose) {
            log::log("=============================");
            log::log("          AST                ");
            log::log("=============================");
            log::log(ast);
        }

        auto intermediate_representation = node::translator{}(ast);

        if(args.verbose) {
            log::log("=============================");
            log::log("             3AC             ");
            log::log("=============================");
            log::log(intermediate_representation);
        }

        if(args.optimizations) {
            intermediate_representation = optimizer{}
                    .verbose(args.verbose)
                    (intermediate_representation);
        }

        if(!args.output_fname.empty()) {
            arch::mix::expander X(args.output_fname);
            X(intermediate_representation);
        }
    }
    catch (custom_exception &r) {
        std::cerr << r.what() << std::endl;
        exit(-1);
    }

}

