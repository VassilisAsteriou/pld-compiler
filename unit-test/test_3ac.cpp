#include <iostream>

#include "test_3ac.hpp"

bool test_3ac::register_flag = Test::UnitTest::registerUnitTest(new test_3ac());

using namespace tac;

#include <list>
#include <cx_simp.hpp>
#include <iomanip>

void test_3ac::instantiation() {

    // For every possible instruction in the IS generate
    // the corresponding object, and log all possible
    // combinations.

    variable_location d{"destvar"}, s1{"s1"}, s2{"s2"};
    code_location target{"label"};
    immediate i1 = 2;
    immediate i2 = 3;

    P.code.emplace_back(nop());

    P.code.emplace_back(direct_assignment{d, s1});
    P.code.emplace_back(direct_assignment{d, i1});
    for(auto op : {neg, lnot}) {
        P.code.emplace_back(unop_assignment{d, s1, op});
        P.code.emplace_back(unop_assignment{d, i1, op});
    }
    for(auto op : {add, sub, mul, tac::div, mod, less, less_or_equal, equal}) {
        P.code.emplace_back(binop_assignment{d, s1, s2, op});
        P.code.emplace_back(binop_assignment{d, i1, s2, op});
        P.code.emplace_back(binop_assignment{d, s1, i2, op});
        P.code.emplace_back(binop_assignment{d, i1, i2, op});
    }
    P.code.emplace_back(unconditional_jump{target});
    P.code.emplace_back(if_zero_jump{target, d});
    P.code.emplace_back(if_nonzero_jump{target, d});
    P.code.emplace_back(if_zero_jump{target, i1});
    P.code.emplace_back(if_nonzero_jump{target, i2});
    P.code.emplace_back(label{target});
    P.code.emplace_back(print{d});
    P.code.emplace_back(print{i2});

    for(auto &i : P.code) log::log(i);

}

void test_3ac::testConstexpr() {

    std::cerr << "Constant Expression Detection" << std::endl;

    for(auto &i : P.code) {
        std::cerr << std::left << std::setw(30) << to_string(i) << std::setw(10) << std::boolalpha << i.isConstexpr()
        << std::setw(30) << to_string(opt::simplify_constant_expression(i)) << std::endl;
    }

}
