#ifndef LANG_TEST_3AC_HPP
#define LANG_TEST_3AC_HPP

#include <UnitTest.hpp>
#include <langlib.hpp>

class test_3ac : Test::UnitTest {

        static bool register_flag;

        tac::program P;

    public:

        void operator() () override {
            instantiation();
            testConstexpr();
        }

    private:

        void instantiation();
        void testConstexpr();

};


#endif //LANG_TEST_3AC_HPP
