#include "parser_test.hpp"

bool parser_test::registration =
        Test::UnitTest::registerUnitTest(new parser_test());

void parser_test::program_literals() {
    /*
     * Try using a literal. This helps because
     * it is a primitive integration test.
     */
    auto p1 = "{var x: int;}."_prog;
    assertEquals(1LU, p1->decls.list[0].size());
    assertEquals(std::string("x"), p1->decls.list[0][0]);

    /*
     * Test if we can use it without the dot.
     */
    auto p2 = "{ var y: int; }"_prog;
    assertEquals(1LU, p2->decls.list[0].size());
    assertEquals(std::string("y"), p2->decls.list[0][0]);

    /*
     * Verify that a syntactically incorrect program
     * yields an empty optional.
     */
    Assert(!"{ 5525 * 4 && 7; }"_prog);
}

void parser_test::assoc_prec() {
    /*
     * Check that all regular operators yield a
     * left-associative AST
     */
    for(const auto &p : {
            "{ print 1 + 2 + 3; }"_prog,
            "{ print 1 - 2 - 3; }"_prog,
            "{ print 1 * 2 * 3; }"_prog,
            "{ print 1 / 2 / 3; }"_prog,
            "{ print 1 % 2 % 3; }"_prog,

            "{ print 1 || 2 || 3; }"_prog,
            "{ print 1 && 2 && 3; }"_prog,

            "{ print 1 == 2 == 3; }"_prog,
            "{ print 1 != 2 != 3; }"_prog,

            "{ print 1 < 2 < 3; }"_prog,
            "{ print 1 <= 2 <= 3; }"_prog,
            "{ print 1 > 2 > 3; }"_prog,
            "{ print 1 >= 2 >= 3; }"_prog,
    }) {
        assertEquals(left, getAssociativity(*p), "Expected left associativity");
    }

    /*
     * Assert that multiple unary operators are admissible
     */
    Assert((bool)"{ var x:int; x = 1 + !-!0; }"_prog);

    /*
     * Check that appropriate precedences apply
     */
    for(auto p : {
        "{ print !0 + 1; }"_prog,
        "{ print 2*3 + 5; }"_prog,
        "{ print 5%7 - 4; }"_prog,
        "{ print 5 * 3 < 16; }"_prog,
        "{ print 2 - 5 >= 0; }"_prog,
        "{ print 1 > 2 == 0; }"_prog,
        "{ print 3 <= 4 != 2; }"_prog,
        "{ print 5 == 1 && 6; }"_prog,
        "{ print !3 && 7; }"_prog,
        "{ print 1 && 2 || 3; }"_prog
    }) {
        try {
            assertEquals(first, getPrecedence(*p), "Expected precedence of the first operator");
        }
        catch (...) {
            Assert(false, "Parsing error.");
        }
    }
    for(auto p : {
            "{ print 1 + !0; }"_prog,
            "{ print 7 + 3*1; }"_prog,
            "{ print 25 - 15/4; }"_prog,
            "{ print 7 > 2 * 3; }"_prog,
            "{ print 2 >= 5 - 25; }"_prog,
            "{ print 1 == 2 <= 0; }"_prog,
            "{ print 0 != 4 < 2; }"_prog,
            "{ print 5 && 1 == 6; }"_prog,
            "{ print 3 && !0; }"_prog,
            "{ print 1 || 2 && 3; }"_prog
    }) {
        try {
            assertEquals(second, getPrecedence(*p), "Expected precedence of the second operator");
        }
        catch (...) {
            Assert(false, "Parsing Error");
        }
    }
}

parser_test::associativity_t parser_test::getAssociativity(const node::program &prog)
{
    auto expression = *std::get<node::print>(
        std::get<node::simple_statement>(*prog.statements[0]))
        .expr;
    auto be = std::get<node::binexp>(expression);
    /*
     * Two cases:
     *  (A) RIGHT | (B) LEFT
     *      $     |        $
     *     / \    |       / \
     *    X  $    |      $  X
     *      / \   |     / \
     *     X  X   |    X  X
     */

    if(std::holds_alternative<node::binexp>(*be.op2)) {
        return right;
    }
    if(std::holds_alternative<node::binexp>(*be.op1)) {
        return left;
    }
    throw std::runtime_error("Hey!");
}

parser_test::precedence_t parser_test::getPrecedence(const node::program &prog)
{
    auto expression = *std::get<node::print>(
            std::get<node::simple_statement>(*prog.statements[0]))
            .expr;
    auto be = std::get<node::binexp>(expression);
    /*
     * Two cases:
     *  (A) SECOND | (B) FIRST
     *      $      |        $
     *     / \     |       / \
     *    X  $     |      $  X
     *      / \    |     / \
     *     X  X    |    X  X
     */

    if(std::holds_alternative<node::int_literal>(*be.op1)) {
        return second;
    }
    if(std::holds_alternative<node::int_literal>(*be.op2)) {
        return first;
    }
    throw std::runtime_error("Hey!");
}
