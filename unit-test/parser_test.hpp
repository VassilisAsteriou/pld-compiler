#ifndef LANG_INPUT_PROGRAM_LITERALS_HPP
#define LANG_INPUT_PROGRAM_LITERALS_HPP

#include <UnitTest.hpp>
#include <fnb.hpp>

class parser_test : public Test::UnitTest {

        static bool registration;

        enum associativity_t {
            left,
            right
        };

        enum precedence_t {
            first,
            second
        };

        static
        associativity_t getAssociativity(node::program const &prog);

        static
        precedence_t getPrecedence(node::program const &prog);

    public:

        void operator() () override {
            program_literals();
            assoc_prec();
        }

        void program_literals();
        void assoc_prec();
};


#endif //LANG_INPUT_PROGRAM_LITERALS_HPP
