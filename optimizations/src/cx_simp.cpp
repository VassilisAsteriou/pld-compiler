
#include <langlib.hpp>

#include "cx_simp.hpp"

namespace opt {

    tac::instruction &simplify_constant_expression(tac::instruction &i) {
        if (i.isConstexpr()) {
            std::visit(utility::visitor{
                [&](auto &i_inn) {
                    std::visit(utility::visitor{
                        [&](tac::unop_assignment &q) {
                            switch (q.op) {
                                case tac::neg:
                                    i = tac::direct_assignment{q.dest, -std::get<tac::immediate>(q.src)};
                                    break;
                                case tac::lnot:
                                    i = tac::direct_assignment{q.dest, !std::get<tac::immediate>(q.src)};
                                    break;
                            }
                        },
                        [&](tac::binop_assignment &q) {
                            switch (q.op) {
                                case tac::add:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) +
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                                case tac::sub:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) -
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                                case tac::mul:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) *
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                                case tac::div:
                                    if(std::get<tac::immediate>(q.src2)) {
                                        i = tac::direct_assignment{q.dest,
                                                                   std::get<tac::immediate>(q.src1) /
                                                                   std::get<tac::immediate>(q.src2)};
                                    }
                                    break;
                                case tac::mod:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) %
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                                case tac::less:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) <
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                                case tac::equal:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) ==
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                                case tac::less_or_equal:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) <=
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                                case tac::not_equal:
                                    i = tac::direct_assignment{q.dest, std::get<tac::immediate>(q.src1) !=
                                                                       std::get<tac::immediate>(q.src2)};
                                    break;
                            }
                        },
                        [&](tac::if_nonzero_jump &j) {
                            if (std::get<tac::immediate>(j.value)) {
                                i = tac::unconditional_jump{j.target};
                            } else {
                                i = tac::nop{};
                            }
                        },
                        [&](tac::if_zero_jump &j) {
                            if (!std::get<tac::immediate>(j.value)) {
                                i = tac::unconditional_jump{j.target};
                            } else {
                                i = tac::nop{};
                            }
                        },
                        [&](auto &) { /* pass */ }
                    }, i_inn);
                }
            }, (tac::instruction::base &) i);
        }
        return i;
    }

}