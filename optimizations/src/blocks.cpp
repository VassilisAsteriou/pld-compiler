
#include <map>
#include <langlib.hpp>

#include "cx_simp.hpp"
#include "label_cleanup.hpp"

#include "blocks.hpp"

namespace opt {

    block get_block(std::list<tac::instruction> &P) {
        static int t = 0;

        block b;
        b.id = tac::get<tac::label>(*P.begin())->loc;
        auto it = P.begin();
        do {
            ++it;
            if(it == P.end()) break;
            if(tac::isa<tac::label>(*it)) break;
            if(tac::isa<tac::if_zero_jump>(*it) ||
                    tac::isa<tac::if_nonzero_jump>(*it)) {
                ++it;
                P.insert(it, tac::label{"__" + std::to_string(t++)});
                --it;
                break;
            }
        } while(true);

        b.instructions.splice(b.instructions.begin(), P, P.begin(), it);

        return b;
    }

    void program::make_graph() {
        for(auto &b: blocks) {
            B[b.id] = &b;
        }

        const block *prev = nullptr;
        for(auto &b: blocks) {
            if(prev) {
                G[prev].insert(&b);
            }
            prev = &b;

            auto &i = b.instructions.back();
            i.visit<void>(
                    utility::visitor{
                            [&](const tac::if_zero_jump &j){
                                G[&b].insert(B[j.target]);
                            },
                            [&](const tac::if_nonzero_jump&j){
                                G[&b].insert(B[j.target]);
                            },
                            [&](const tac::unconditional_jump &j){
                                G[&b].insert(B[j.target]);
                                prev = nullptr;
                            },
                            [&](const auto &) { /* pass */ }
                    });
        }
    }

    program split(tac::program &p) {
        program q;

        cleanup_labels(p);

        q.decls = std::move(p.static_alloc);

        p.code.insert(p.code.begin(), tac::label{"__init__"});
        while(!p.code.empty()) {
            q.blocks.push_back(get_block(p.code));
        }

        return q;
    }

    tac::program merge(program &q) {
        tac::program p;
        p.static_alloc = std::move(q.decls);

        for(auto &b : q.blocks) {
            p.code.splice(p.code.end(), std::move(b.instructions));
        }

        cleanup_labels(p);

        /*
         * Cleanup data
         */
        std::set<tac::variable_location> used_vars;
        for(auto &i : p.code) {
            auto u = i.use();
            used_vars.insert(u.begin(), u.end());
            auto d = i.def();
            if(d) used_vars.insert(*d);
        }
        auto it = p.static_alloc.begin();
        while(it != p.static_alloc.end()) {
            if(used_vars.find(it->loc) == used_vars.end()) {
                it = p.static_alloc.erase(it);
            } else {
                ++it;
            }
        }

        return p;
    }

    void block::local_data_propagation() {
        std::map<tac::variable_location, tac::data_operand> V;

        auto substitute = [&](tac::data_operand &d) {
            if(std::holds_alternative<tac::variable_location>(d)) {
                auto i = V.find(std::get<tac::variable_location>(d));
                if(i != V.end()) {
                    d = i->second;
                }
            }
        };

        for(auto &i: instructions) {
            i.visit<void>(utility::visitor{
                [&](tac::direct_assignment &d) {
                    substitute(d.src);
                },
                [&](tac::unop_assignment &u) {
                    substitute(u.src);
                },
                [&](tac::binop_assignment &b) {
                    substitute(b.src1);
                    substitute(b.src2);
                },
                [&](tac::if_zero_jump &i) {
                    substitute(i.value);
                },
                [&](tac::if_nonzero_jump &i) {
                    substitute(i.value);
                },
                [&](tac::print &p){
                    substitute(p.op);
                },
                [](const auto &) { /* pass */ }
            });
            simplify_constant_expression(i);
            i.visit<void>(utility::visitor{
                [&](const tac::direct_assignment &a) {
                    if(std::holds_alternative<tac::immediate>(a.src)) {
                        V[a.dest] = a.src;
                    }
                },
                [&](const auto &) { /* pass */ }
            });
        }
    }

    void block::post_goto_elimination() {
        auto it = instructions.begin();
        for(; it != instructions.end(); ++it) {
            if(tac::isa<tac::unconditional_jump>(*it)) {
                ++it;
                break;
            }
        }
        for(; it != instructions.end();) {
            it = instructions.erase(it);
        }
    }

    void block::optimize_locally() {
        local_data_propagation();
        post_goto_elimination();
    }
}