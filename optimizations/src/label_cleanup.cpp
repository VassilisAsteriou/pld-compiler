
#include <map>

#include "label_cleanup.hpp"

namespace opt {

    tac::program &cleanup_labels(tac::program &in) {
        /*
         * Label cluster merger
         */
        if (in.code.size() >= 2) {
            auto &c = in.code;

            std::map<tac::code_location, tac::code_location> S;

            auto next = c.begin();
            auto instr = next++;

            /*
             * Find label pools and keep only the primary.
             * Point the rest to the primary
             */
            while (next != c.end()) {
                if (tac::isa<tac::label>(*instr) && tac::isa<tac::label>(*next)) {
                    auto &src = tac::get<tac::label>(*next)->loc;
                    auto &dest = tac::get<tac::label>(*instr)->loc;
                    S[src] = dest;
                    next = c.erase(next);
                } else {
                    instr = next;
                    next++;
                }
            }

            /*
             * Swap all references to non-primary labels to
             * the corresponding primary.
             */
            for (auto &i : c) {
                if (auto x = tac::get<tac::unconditional_jump>(i)) {
                    if (S.find(x->target) != S.end()) {
                        i = tac::unconditional_jump{S[x->target]};
                    }
                } else if (auto y = tac::get<tac::if_zero_jump>(i)) {
                    if (S.find(y->target) != S.end()) {
                        i = tac::if_zero_jump{S[y->target], y->value};
                    }
                } else if (auto z = tac::get<tac::if_nonzero_jump>(i)) {
                    if (S.find(z->target) != S.end()) {
                        i = tac::if_nonzero_jump{S[z->target], z->value};
                    }
                }
            }
        }

        /*
         * Unused targets discard
         */
        std::set<tac::code_location> used_labels;
        for(auto &i: in.code) {
            i.visit<void>(utility::visitor{
                [&] (const tac::unconditional_jump &j) {
                    used_labels.insert(j.target);
                },
                [&] (const tac::if_nonzero_jump &j) {
                    used_labels.insert(j.target);
                },
                [&] (const tac::if_zero_jump &j) {
                    used_labels.insert(j.target);
                },
                [] (const auto&) { /* pass */ }
            });
        }
        auto it = in.code.begin();
        while(it != in.code.end()) {
            auto l = tac::get<tac::label>(*it);
            if(l) {
                if(used_labels.find(l->loc) == used_labels.end()) {
                    it = in.code.erase(it);
                    continue;
                }
            }
            ++it;
        }

        return in;
    }
}