#include <langlib.hpp>

#include "label_cleanup.hpp"
#include "cx_simp.hpp"
#include "blocks.hpp"
#include "use_def.hpp"

#include "optimizer.hpp"

using namespace opt;

tac::program &optimizer::operator()(tac::program &in) {
    size_t nol = 0;

    do {
        nol = in.code.size();

        auto q = split(in);

        for (auto &b: q.blocks) {
            b.optimize_locally();
        }
        q.make_graph();

        ud_analyzer{q}.optimize();

        in = merge(q);

        /*
         * Fallthrough optimization
         */
        if(in.code.size() >= 2) {
            auto i1 = in.code.begin();
            auto i2 = ++in.code.begin();

            while(i2 != in.code.end()) {
                auto l = tac::get<tac::label>(*i2);
                if(l) {
                    auto loc = l->loc;
                    if (i1->visit<bool>(utility::visitor{
                            [&](const tac::if_zero_jump &j) {
                                return j.target == loc;
                            },
                            [&](const tac::if_nonzero_jump &j) {
                                return j.target == loc;
                            },
                            [&](const tac::unconditional_jump &j) {
                                return j.target == loc;
                            },
                            [&](const auto &) { return false; }
                    })) {
                        *i1 = tac::nop{};
                    }
                }

                ++i1;
                ++i2;
            }
        }

        for (auto it = in.code.begin(); it != in.code.end();) {
            if (tac::isa<tac::nop>(*it)) {
                it = in.code.erase(it);
            } else {
                ++it;
            }
        }

        if(verbose_) {
            log::log("==========================");
            log::log("  Optimization iteration  ");
            log::log("==========================");
            log::log(in);
        }
    } while(nol != in.code.size());

    return in;
}

optimizer &optimizer::verbose(bool v) {
    verbose_ = v;
    return *this;
}
