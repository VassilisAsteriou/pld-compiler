#include <map>

#include "use_def.hpp"

namespace opt {

    ud_analyzer::ud_analyzer(program &p) :
        p{p}
    {
        // Map of a variable to its plausible defs
        using use_t = std::map<tac::variable_location, std::set<const tac::instruction *>>;

        // Map of a variable to its last unused def.
        using def_t = std::map<tac::variable_location, const tac::instruction *>;

        std::map<block const *, use_t> USE;
        std::map<block const *, def_t> DEF;

        /*
         * For each block construct its USE and DEF maps.
         * A def map contains all unused definitions of the block
         * A use map contains all not-yet defined uses in the block
         * Locally shadowed definitions are reported.
         */
        for (auto &b: p.blocks) {
            auto &U = USE[&b];
            auto &D = DEF[&b];

            for (auto &i : b.instructions) {
                for (auto &v : i.use()) {
                    if (D.find(v) != D.end()) {
                        D.erase(v);
                    } else {
                        U[v] = {};
                    }
                }
                auto v = i.def();
                if (v) {
                    auto f = D.find(*v);
                    if(f != D.end()) {
                        unused_definitions.insert(f->second);
                    }
                    D[*v] = &i;
                }
            }
        }

        /*
         * Compute the closure of the USE sets.
         */
        for(int i = 0; i < p.G.size(); ++i) {
            for(auto &pair: p.G) {
                auto &A = pair.first;
                for(auto &B : pair.second) {
                    for(auto &vs : USE[B]) {
                        if(DEF[A].find(vs.first) != DEF[A].end()) {
                            USE[B][vs.first].insert(DEF[A][vs.first]);
                        }
                        else {
                            USE[A][vs.first] = {};
                        }
                    }
                }
            }
        }

        /*
         * Resolve unused definition/undefined use pairs.
         */
        for(auto &pair: p.G) {
            auto &A = pair.first;
            for(auto &B : pair.second) {
                for(auto &vs: DEF[A]){
                    auto &U = USE[B];
                    auto it = U.find(vs.first);
                    if(it != U.end()) {
                        it->second.insert(vs.second);
                        // Distinguish definition as used, but do
                        // not discard it.
                        DEF[A][vs.first] = nullptr;
                    }
                }
            }
        }

        /*
         * The remaining unused definitions are redundant.
         */
        for(auto &b: p.blocks) {
            for(auto &vs: DEF[&b]) {
                if(vs.second)
                    unused_definitions.insert(vs.second);
            }
        }
    }

    void ud_analyzer::optimize() {
        for(auto &i : unused_definitions) {
            *const_cast<tac::instruction*>(i) = tac::nop{};
        }
    }

}