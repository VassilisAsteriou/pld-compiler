#ifndef LANG_CX_SIMP_H
#define LANG_CX_SIMP_H

#include <langlib.hpp>

namespace opt {

    tac::instruction &simplify_constant_expression(tac::instruction &);

}

#endif //LANG_CX_SIMP_H
