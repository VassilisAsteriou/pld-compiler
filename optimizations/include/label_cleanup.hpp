#ifndef LANG_LABEL_CLEANUP_H
#define LANG_LABEL_CLEANUP_H

#include <langlib.hpp>

namespace opt {

    tac::program &cleanup_labels(tac::program &in);

}

#endif //LANG_LABEL_CLEANUP_H
