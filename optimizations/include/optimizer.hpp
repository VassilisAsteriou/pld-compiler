#ifndef LANG_OPTIMIZER_H
#define LANG_OPTIMIZER_H

#include <langlib.hpp>

class optimizer {

        bool verbose_ = false;

    public:
    
        tac::program &operator() (tac::program &in);

        optimizer &verbose(bool verbosity);
};


#endif //LANG_OPTIMIZER_H
