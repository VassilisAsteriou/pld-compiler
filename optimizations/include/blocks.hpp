#ifndef LANG_BLOCKS_H
#define LANG_BLOCKS_H

#include <langlib.hpp>
#include <map>

namespace opt {

    struct block {
        tac::code_location id;
        std::list<tac::instruction> instructions;

        void optimize_locally();

        void local_data_propagation();
        void post_goto_elimination();
    };

    struct program {
        std::list<tac::static_allocation> decls;
        std::list<block> blocks;

        std::map<tac::code_location, block*> B;
        std::map<const block*, std::set<const block*>> G;

        void make_graph();
    };

    program split(tac::program &p);
    tac::program merge(program &p);
}

#endif //LANG_BLOCKS_H
