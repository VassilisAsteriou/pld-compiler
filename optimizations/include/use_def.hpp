#ifndef LANG_SSA_H
#define LANG_SSA_H

#include "blocks.hpp"
#include <langlib.hpp>

namespace opt {

    class ud_analyzer {

            using instr = const tac::instruction *;

            std::set<instr> unused_definitions;

            program &p;

        public:

            explicit ud_analyzer(program &p);

            void optimize();

    };

}

#endif //LANG_SSA_H
