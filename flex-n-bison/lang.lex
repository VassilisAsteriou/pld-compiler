%option noyywrap
%option yylineno
%option reentrant

%option extra-type="scanner_extra *"

%{
#include <string>

#include "fnb.hpp"
#include "lang.hpp"

int yyget_leng ( yyscan_t yyscanner );
int yyget_lineno ( yyscan_t yyscanner );
scanner_extra *yyget_extra(void *);

void update_location(void *yyscanner) {
    yy::location &current_location = yyget_extra(yyscanner)->current_location;
    current_location.step();
    if(yyget_lineno(yyscanner) == current_location.end.line) {
        current_location.columns(yyget_leng(yyscanner));
    }
    else {
        current_location.lines();
    }
}

void get_input(char buf[], int &result, int maxsize, void *yyscanner) {
    std::istream *&in = yyget_extra(yyscanner)->in;
    char c;
    if ( in->get(c) ) {
        buf[0] = c;
        result = 1;
    } else {
        result = 0;
    }
}

#define YY_INPUT(buf, result, max_size) get_input(buf, result, max_size, yyscanner);
#define YY_USER_ACTION update_location(yyscanner);

%}

digit [0-9]
nonzero [1-9]
letter [a-zA-Z]

number (0|{nonzero}{digit}*)
identifier {letter}({letter}|{digit}|_)*

%%

[ \n\t]     /* Skip whitespace */

    /* Keywords */

var         return yy::parser::make_VAR(yyextra->current_location);
int         return yy::parser::make_INT(yyextra->current_location);
print       return yy::parser::make_PRINT(yyextra->current_location);

if          return yy::parser::make_IF(yyextra->current_location);
while       return yy::parser::make_WHILE(yyextra->current_location);
for         return yy::parser::make_FOR(yyextra->current_location);
continue    return yy::parser::make_CONTINUE(yyextra->current_location);
break       return yy::parser::make_BREAK(yyextra->current_location);
else        return yy::parser::make_ELSE(yyextra->current_location);

    /* Delimiters */

"{"         return yy::parser::make_LBRACE(yyextra->current_location);
"}"         return yy::parser::make_RBRACE(yyextra->current_location);
";"         return yy::parser::make_SEMICOLON(yyextra->current_location);
":"         return yy::parser::make_COLON(yyextra->current_location);
","         return yy::parser::make_COMMA(yyextra->current_location);
"("         return yy::parser::make_LPAREN(yyextra->current_location);
")"         return yy::parser::make_RPAREN(yyextra->current_location);

    /* Operators */

"+="        return yy::parser::make_ASSIGN_ADD(yyextra->current_location);
"-="        return yy::parser::make_ASSIGN_SUB(yyextra->current_location);
"*="        return yy::parser::make_ASSIGN_MUL(yyextra->current_location);
"/="        return yy::parser::make_ASSIGN_DIV(yyextra->current_location);
"%="        return yy::parser::make_ASSIGN_MOD(yyextra->current_location);

"+"         return yy::parser::make_PLUS(yyextra->current_location);
"-"         return yy::parser::make_MINUS(yyextra->current_location);
"*"         return yy::parser::make_MUL(yyextra->current_location);
"/"         return yy::parser::make_DIV(yyextra->current_location);
"%"         return yy::parser::make_MOD(yyextra->current_location);

"<"         return yy::parser::make_LT(yyextra->current_location);
"<="        return yy::parser::make_LE(yyextra->current_location);
">"         return yy::parser::make_GT(yyextra->current_location);
">="        return yy::parser::make_GE(yyextra->current_location);
"=="        return yy::parser::make_EQ(yyextra->current_location);
"!="        return yy::parser::make_NE(yyextra->current_location);

"="         return yy::parser::make_ASSIGN(yyextra->current_location);

"&&"        return yy::parser::make_AND(yyextra->current_location);
"||"        return yy::parser::make_OR(yyextra->current_location);
"!"         return yy::parser::make_NOT(yyextra->current_location);

{number}        return yy::parser::make_NUMBER(std::stoi(std::string(yytext, yyleng)), yyextra->current_location);
{identifier}    return yy::parser::make_IDENTIFIER(std::string(yytext, yyleng), yyextra->current_location);

<<EOF>>     return yy::parser::make_END(yyextra->current_location);

"."         return yy::parser::make_END(yyextra->current_location);

.           {
                log::error("Invalid character" + std::to_string((int) *yytext));
                log::error("Aborting...");
                return yy::parser::make_END(yyextra->current_location);
            }


%%