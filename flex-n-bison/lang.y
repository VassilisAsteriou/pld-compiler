%skeleton "lalr1.cc"
%define api.value.type variant
%define api.token.constructor

%define parse.trace

%parse-param { node::program &return_ }
%param { void *yyscanner }

// Included in parser header
%code requires {
#include <langlib.hpp>

#include <fstream>

#define YY_DECL yy::parser::symbol_type yylex(void* yyscanner)
}

// Included in parser source
%code {

YY_DECL;

#define YYDEBUG 1
}

%locations

%token <std::string> IDENTIFIER
%token <int32_t> NUMBER

//%token <keyword> VAR INT PRINT IF WHILE FOR CONTINUE BREAK ELSE
%token VAR INT PRINT IF WHILE FOR CONTINUE BREAK ELSE
%token LBRACE RBRACE LPAREN RPAREN COMMA COLON SEMICOLON

%token END 0 "end of file"

%type <node::expression> exp binexp unexp
%type <node::operator_t> asop
%type <node::simple_statement> simp
%type <node::block> stmts block else-blc
%type <node::statement> stmt control
%type <node::variables_list> vars decl
%type <node::declarations> decls

%right ASSIGN ASSIGN_ADD ASSIGN_SUB ASSIGN_MUL ASSIGN_DIV ASSIGN_MOD

%left OR
%left AND
%left EQ NE
%left LT LE GT GE
%left PLUS MINUS
%left MUL DIV MOD

%right NOT UNARY

%%

%start program;

program : LBRACE decls stmts RBRACE
        {
            return_ = node::program{$2, $3};
        }

decls   : decls decl
        {
            $1.list.push_back($2);
            $$ = $1;
        }
        |
        {
            $$ = node::declarations{ { } };
        }

decl    : VAR IDENTIFIER vars COLON type SEMICOLON
        {
            $3.push_back($2);
            $$ = $3;
        }

vars    : COMMA IDENTIFIER vars
        {
            $3.push_back($2);
            $$ = $3;
        }
        |
        {
            $$ = {};
        }

type    : INT

stmts   : stmts stmt
        {
            if($2)
                $1.push_back($2);
            $$ = $1;
        }
        |
        {
            $$ = {};
        }

stmt    : simp SEMICOLON
        {
            $$ = std::make_unique<node::statement_>(std::move($1));
        }
        | control
        {
            $$ = std::move($1);
        }
        | SEMICOLON
        {
            //$$ = nullptr;
        }

simp    : IDENTIFIER asop exp
        {
            node::lvalue l = {$1};
            $$ = node::assignment{l, $2, std::move($3)};
        }
        | PRINT exp
        {
            $$ = node::print{std::move($2)};
        }

control : IF LPAREN exp RPAREN block else-blc
        {
            node::_if c{std::move($3), std::move($5), std::move($6)};
            $$ = std::make_unique<node::statement_>(std::move(c));
        }
        | WHILE LPAREN exp RPAREN block
        {
            node::_while c{std::move($3), $5};
            $$ = std::make_unique<node::statement_>(std::move(c));
        }
        | FOR LPAREN simp SEMICOLON exp SEMICOLON simp RPAREN block
        {
            node::_for c{std::move($3), std::move($5), std::move($7), std::move($9)};
            $$ = std::make_unique<node::statement_>(std::move(c));
        }
        | CONTINUE SEMICOLON
        {
            $$ = std::make_unique<node::statement_>(node::_continue());
        }
        | BREAK SEMICOLON
        {
            $$ = std::make_unique<node::statement_>(node::_break());
        }

else-blc : ELSE block
        {
            $$ = $2;
        }
        |
        {
            $$ = {};
        }

block   : stmt
        {
            $$ = {std::move($1)};
        }
        | LBRACE stmts RBRACE
        {
            $$ = $2;
        }

exp     : LPAREN exp RPAREN
        {
            $$ = std::move($2);
        }
        | NUMBER
        {
            node::int_literal l{$1};
            $$ = std::make_unique<node::expression_>(l);
        }
        | IDENTIFIER
        {
            node::rvalue r{$1};
            $$ = std::make_unique<node::expression_>(r);
        }
        | unexp
        {
            $$ = std::move($1);
        }
        | binexp
        {
            $$ = std::move($1);
        }

asop    : ASSIGN        { $$ = node::assign; }
        | ASSIGN_ADD    { $$ = node::add_assign; }
        | ASSIGN_SUB    { $$ = node::sub_assign; }
        | ASSIGN_MUL    { $$ = node::mul_assign; }
        | ASSIGN_DIV    { $$ = node::div_assign; }
        | ASSIGN_MOD    { $$ = node::mod_assign; }

binexp  : exp PLUS exp
        {
            node::operator_t op = node::add;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp MINUS exp
        {
            node::operator_t op = node::sub;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp MUL exp
        {
            node::operator_t op = node::mul;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp DIV exp
        {
            node::operator_t op = node::div;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp MOD exp
        {
            node::operator_t op = node::mod;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp LT exp
        {
            node::operator_t op = node::less;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp LE exp
        {
            node::operator_t op = node::less_equal;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp GT exp
        {
            node::operator_t op = node::greater;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp GE exp
        {
            node::operator_t op = node::greater_equal;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp EQ exp
        {
            node::operator_t op = node::equal;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp NE exp
        {
            node::operator_t op = node::not_equal;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp AND exp
        {
            node::operator_t op = node::logic_and;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | exp OR exp
        {
            node::operator_t op = node::logic_or;
            node::binexp b{std::move($1), std::move($3), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }

unexp   : NOT exp
        {
            node::operator_t op = node::logic_not;
            node::unexp b{std::move($2), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }
        | MINUS exp %prec UNARY
        {
            node::operator_t op = node::neg;
            node::unexp b{std::move($2), op};
            $$ = std::make_unique<node::expression_>(std::move(b));
        }

%%

#include "fnb.hpp"

using std::to_string;

inline
std::string calculate_source_line(const std::string &source_name, unsigned int lineno) {
    int N = lineno;
    std::ifstream source(source_name);
    std::string in;
    while(source.good() && N--) {
        getline(source, in);
    }
    return in;
}

void yy::parser::error(const location_type& loc, const std::string &msg) {
    std::string loc_str = to_string(loc.begin.line) + ":" + to_string(loc.begin.column);

    log::error("at " + loc_str + ": " + msg);
    if(loc.begin.filename) {
        std::string source_line = calculate_source_line(*loc.begin.filename, loc.begin.line);
        log::log(source_line);
        log::log(std::string(loc.begin.column - 1, ' ')
                 + '^' + std::string(loc.end.column - loc.begin.column - 1, '~'));
    }
    throw parsing_error();
}