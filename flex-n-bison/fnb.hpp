#ifndef FNB_HPP
#define FNB_HPP

/*
 * All of the following includes are actually redundant
 * but they are here nevertheless to (a) make dependencies
 * visible and (b) disable misunderstandings with the IDE.
 */

#include <string>
#include <fstream>
#include <langlib.hpp>

#include "lang.hpp"

struct scanner_extra {
    std::istream *in;
    yy::location current_location;
};

/*
 *  These are implemented in lang.lex, but since it has no
 *  header, they are extern-ed here.
 */
extern int yylex_init_extra(scanner_extra *, void**);
extern int yylex_destroy(void*);

class driver {

        node::program prog;
        scanner_extra extra;

    public:
        explicit driver(const std::string &in) {
            if (in.empty()) {
                throw no_input_file();
            }
            auto in_ = new std::ifstream{in};
            if(in_->fail()) throw no_input_file();
            extra.in = in_;
        }

        explicit driver(std::istream *in) {
            extra.in = in;
        }

        ~driver() {
            delete extra.in;
        }

        node::program operator()() {

            void *yyscanner;
            yylex_init_extra(&extra, &yyscanner);

            yy::parser parser{prog, yyscanner};
            parser.parse();

            yylex_destroy(yyscanner);
            return prog;
        }
};

std::optional<node::program> operator "" _prog(const char *s, size_t size);

#endif //FNB_HPP
