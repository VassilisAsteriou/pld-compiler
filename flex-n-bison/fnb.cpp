#include <sstream>
#include "fnb.hpp"

std::optional<node::program> operator "" _prog(const char *s, size_t size) {
    std::string string{s, size};
    auto in = new std::stringstream{string};
    driver d{in};
    try {
        return d();
    }
    catch (custom_exception &r) {
        std::cerr << r.what() << std::endl;
        return {};
    }
}

