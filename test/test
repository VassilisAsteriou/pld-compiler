#!/usr/bin/python3

import os
import sys
import argparse
import subprocess
import itertools
import ruamel.yaml
import jinja2
import hashlib
import pathlib
from progress.bar import Bar

import math
import random

def dict_product(**kwargs):
    keys = kwargs.keys()
    vals = kwargs.values()
    for instance in itertools.product(*vals):
        yield dict(zip(keys, instance))


def get_cross_size(d):
    size = 1
    for k in d:
        size *= len(eval(d[k]))
    return int(size)


def is_prime(n):
    if n == 1:
        return False
    if n == 2:
        return True
    return not(any([n % q == 0 for q in range(2, n - 1)]))


test_space = {
    'is_prime': is_prime,
    'math': math,
    'random': random
}


class Failure(Exception):
    def __init__(self, failure):
        self.fail = failure


class TestFailure(Failure):
    pass


class OptimizationFailure(Failure):
    pass


class Tests:

    env = jinja2.Environment(loader=jinja2.FileSystemLoader('./'))

    def __init__(self, input_file_name,
                 compiler_executable='../cmake-build-debug/flex-n-bison/lang'):
        self.compiler = compiler_executable

        i = open(input_file_name)
        suite = ruamel.yaml.YAML().load(i)
        i.close()
        self.tests = suite['tests']

        self.basedir = str(pathlib.Path(input_file_name).parent)
        basedir = self.basedir
        self.tmp_dir = basedir + '/tmp/'
        self.asm_dir = basedir + '/mix/asm/'
        self.bin_dir = basedir + '/mix/bin/'
        self.report_dir = basedir + '/mix/report/'
        self.env = jinja2.Environment(loader=jinja2.FileSystemLoader(basedir))

    def run(self):
        self.setup()
        failure = False
        for test in self.tests:
            test = {**test}
            try:
                if 'parameters' in test.keys():
                    self.run_parameterized(test)
                else:
                    self.run_regular(test)
            except TestFailure as r:
                print('Test gave unexpected results:')
                print(r.fail)
                failure = True
            except OptimizationFailure as r:
                print('Optimization altered result of execution:')
                print(r.fail)
                failure = True
        return failure

    def template(self, filename, kwargs):
        t = self.env.get_template(filename).render(**kwargs)
        basename = filename.split('/')[-1].split('.')[0]
        tmp_name = self.tmp_dir + basename + hashlib.sha256(t.encode('utf-8')).hexdigest()[0:8] + '.lang'
        f = open(tmp_name, "w")
        f.write(t)
        f.close()
        return tmp_name

    def setup(self):
        for path in [self.tmp_dir, self.asm_dir, self.bin_dir]:
            if not os.path.exists(path):
                os.makedirs(path)

    def toolchain(self, lang_file, optimization=False):
        basename = lang_file.split('/')[-1].split('.')[0]

        asm_file = self.asm_dir + basename + '.mixal'
        bin_file = self.bin_dir + basename + '.mix'

        try:
            if optimization:
                subprocess.check_call([self.compiler, lang_file, '-o', asm_file, '-O1'])
            else:
                subprocess.check_call([self.compiler, lang_file, '-o', asm_file])
            subprocess.check_call(['mixasm', asm_file, '-o', bin_file])
            out = subprocess.check_output(['mixvm', '--run', bin_file])

            subprocess.call(['rm', asm_file, bin_file])
            return Tests.process_result(out)
        except subprocess.CalledProcessError as err:
            print(str(err.cmd) + ' failed with exit code ' + str(err.returncode))

        return None

    @staticmethod
    def process_result(report):
        lines = report.decode('utf-8').split('\n')[2:-2]
        try:
            return [int(line) for line in lines]
        except ValueError:
            return 'overflow'

    @staticmethod
    def failure(name, input_file_name, actual, expected, params):
        print()
        raise TestFailure({'name': name, 'input': input_file_name,
                           'actual': actual, 'expected': expected,
                           'parameters': params})

    @staticmethod
    def optimization_failure(name, input_file_name, regular, optimized, parameters):
        print()
        raise OptimizationFailure({'name': name, 'input': input_file_name,
                                   'regular': regular, 'optimized': optimized,
                                   'parameters': parameters})

    def run_parameterized(self, test):
        print(test['name'])

        input_file_name = self.basedir + '/' + test['input']
        ps = test['parameters']
        b = Bar('', max=get_cross_size(ps), suffix='%(percent)d%%')

        for parameters in dict_product(**{k: eval(ps[k]) for k in ps}):

            tmp = self.template(test['input'], parameters)
            actual = self.toolchain(tmp)
            optimized = self.toolchain(tmp, True)
            subprocess.call(['rm', tmp])
            expected = [eval(x, test_space, parameters) for x in test['output']]

            if actual != expected:
                self.failure(test['name'], input_file_name, actual, expected, parameters)
            elif actual != optimized:
                self.optimization_failure(test['name'], input_file_name, actual, optimized, parameters)

            b.next()

        b.finish()

    def run_regular(self, test):
        print(test['name'])

        input_file_name = self.basedir + '/' + test['input']
        actual = self.toolchain(input_file_name)
        optimized = self.toolchain(input_file_name, True)
        expected = test['output']

        if expected == 'None':
            expected = None

        if actual != expected:
            self.failure(test['name'], input_file_name, actual, expected, {})
        elif actual != optimized:
            self.optimization_failure(test['name'], input_file_name, actual, optimized, {})


arguments_parser = argparse.ArgumentParser(description='Perform test runs on the compiler')
arguments_parser.add_argument('--compiler', '-c',
                              default='cmake-build-debug/lang',
                              help='path to the compiler executable')
arguments_parser.add_argument('test_files', nargs='+', help='YAML test suites')

args = arguments_parser.parse_args(sys.argv[1:])

fail = False
for file in args.test_files:
    print("Test file: " + file)
    if Tests(file, args.compiler).run():
        fail = True

exit(fail)
