#include <utility>

#ifndef LANG_TAC_INSTR_HPP
#define LANG_TAC_INSTR_HPP

#include <string>
#include <variant>
#include <list>
#include <utility.hpp>
#include <set>
#include "logger.hpp"

namespace tac {

    /*
     * Types of instructions:
     *  1. Assignment
     *      a. Without processing
     *      b. With unary operation
     *      c. With binary operation
     *  2. Jump
     *      a. Unconditional
     *      b. If zero
     *      c. If non-zero
     *  3. Locations
     *      a. Variable allocation
     *      b. Code label
     *  4. Miscellaneous
     *      a. Print
     *      b. Nop
     */

    /*
     * Preliminary definitions
     */

    struct string_location : public std::string {
        using base = std::string;
        using base::base;

        string_location(const std::string &s) : base{s} {} // NOLINT

        string_location(const string_location &) = default;
        string_location(string_location &&) = default;
        string_location &operator=(const string_location &) = default;
        string_location &operator=(string_location &&) = default;
    };

    struct code_location : string_location {};
    struct variable_location : string_location {};  // Implemented non-generically

    using immediate = int32_t;
    using data_operand = std::variant<variable_location, immediate>;

    /*
     * Instructions
     */

    enum unop {
        neg,
        lnot
    };

    enum binop {
        add,
        sub,
        mul,
        div,
        mod,

        less,
        equal,
        less_or_equal,
        not_equal
    };

    struct label {
        code_location loc;
    };

    struct nop {};

    struct print {
        data_operand op;
    };

    struct generic_assignment {
        variable_location dest;
    };

    struct direct_assignment : generic_assignment {
        data_operand src;
    };

    struct unop_assignment : generic_assignment {
        data_operand src;
        unop op;
    };

    struct binop_assignment : generic_assignment {
        data_operand src1;
        data_operand src2;
        binop op;

        // Normalize commutative operations
        // so that immediates are first
        void normalize();
    };

    struct unconditional_jump {
        code_location target;
    };

    struct if_zero_jump {
        code_location target;
        data_operand value;
    };

    struct if_nonzero_jump {
        code_location target;
        data_operand value;
    };

    using assignment    = std::variant<direct_assignment, unop_assignment, binop_assignment>;
    using jump          = std::variant<unconditional_jump, if_zero_jump, if_nonzero_jump, label>;
    using miscellaneous = std::variant<nop, print>;

    /*
     * This class represents a runtime instruction
     */
    class instruction : public std::variant<assignment, jump, miscellaneous> {
        public:
            using base = std::variant<assignment, jump, miscellaneous>;
            using base::base;

            instruction() : base{nop()} {}

            operator base const &() const;

            template<class return_t, class visitor_t>
            return_t visit(visitor_t &&visitor) {
                return std::visit([&](auto &a) -> return_t {
                    return std::visit(std::move(visitor), a);
                    }, (base&) *this);
            }

            template<class return_t, class visitor_t>
            return_t visit(visitor_t &&visitor) const {
                return std::visit([&](auto const &a) -> return_t {
                    return std::visit(std::move(visitor), a);
                }, (base const&) *this);
            }

            bool isConstexpr();

            std::set<variable_location> use() const;
            std::optional<variable_location> def() const;
            bool dispensible() const;
    };

    struct static_allocation {
        variable_location loc;
    };

    struct program {
        std::list<instruction> code;
        std::list<static_allocation> static_alloc;
    };

    template<class U>
    bool isa(tac::instruction const &i) {
        return std::visit([](auto const &j) -> bool {
            return std::visit(utility::visitor{
                [](const U &) { return true; },
                [](const auto &) { return false; }
                }, j);
        }, (tac::instruction::base const &) i);
    }

    template<class U>
    std::optional<U> get(tac::instruction i) {
        return std::visit([](auto j) -> std::optional<U> {
            return std::visit(utility::visitor{
                [](U u) -> std::optional<U> { return u; },
                [](auto other) -> std::optional<U> { return std::nullopt; }
            }, j);
        }, (tac::instruction::base) i);
    }
}

#endif //LANG_TAC_INSTR_HPP
