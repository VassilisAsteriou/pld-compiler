#ifndef LANG_MIX_EXPANDER_HPP
#define LANG_MIX_EXPANDER_HPP

#include <string>
#include <fstream>
#include <list>
#include "3ac_instruction.hpp"

namespace arch {

    namespace mix {

        class expander {

                std::ofstream out;

                tac::immediate MAX_INT;
                tac::immediate MAX_ADDRESS = 4095;

                static const std::string tab;

                void LDA(const tac::data_operand &);
                void LDX(const tac::data_operand &);
                void STA(const tac::variable_location &);
                void STX(const tac::variable_location &);

                void LDAN(const tac::data_operand &);
                void ADD(const tac::data_operand &);
                void SUB(const tac::data_operand &);
                void MUL(const tac::data_operand &);
                void DIV(const tac::data_operand &);
                void CMPX(const tac::data_operand &);

                template <class ...Args>
                void expand(const std::variant<Args...> &v);

                void expand(const tac::instruction &i);

                void expand(const tac::label &);
                void expand(const tac::direct_assignment &);
                void expand(const tac::unop_assignment &);
                void expand(const tac::binop_assignment &);
                void expand(const tac::unconditional_jump &);
                void expand(const tac::if_zero_jump &);
                void expand(const tac::if_nonzero_jump &);
                void expand(const tac::nop &);
                void expand(const tac::print &);
                
                void preprocess_labels(tac::program &p);

            public:

                explicit expander(std::ofstream &&out);
                explicit expander(const std::string &out_file_name);

                void operator() (tac::program &);

            void print_overflow_exception();

            void print_message(std::string label, std::string msg);
        };

    }
}

#endif //LANG_MIX_EXPANDER_HPP
