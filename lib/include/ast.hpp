#ifndef LANG_AST_HPP
#define LANG_AST_HPP

#include <cstdint>
#include <memory>
#include <string>
#include <vector>
#include <variant>
#include "3ac_instruction.hpp"

namespace node {

    enum operator_t {
        none,

        add,
        sub,
        mul,
        div,
        mod,
        neg,

        assign,
        add_assign,
        sub_assign,
        mul_assign,
        div_assign,
        mod_assign,

        less,
        greater,
        less_equal,
        greater_equal,
        equal,
        not_equal,

        logic_and,
        logic_or,
        logic_not
    };

    std::string to_string(operator_t);

    struct expression_;

    using expression = std::shared_ptr<expression_>;

    struct rvalue {
        std::string name;
    };

    struct int_literal {
        int32_t value;
    };

    struct binexp {
        expression op1;
        expression op2;
        node::operator_t op;
    };

    struct unexp {
        expression op1;
        node::operator_t op;
    };

    struct expression_ : public std::variant<rvalue, int_literal, binexp, unexp> {
        using base = std::variant<rvalue, int_literal, binexp, unexp>;
        using base::base;
    };

    struct lvalue {
        std::string name;
    };

    struct statement_;

    using statement = std::shared_ptr<statement_>;
    using block = std::vector<statement>;

    struct assignment {
        lvalue var;
        node::operator_t op;
        expression expr;
    };

    struct print {
        expression expr;
    };

    using simple_statement = std::variant<assignment, print>;

    struct _if {
        expression condition;
        block if_block;
        block else_block;
    };

    struct _while {
        expression condition;
        block while_block;
    };

    struct _for {
        simple_statement init;
        expression condition;
        simple_statement update;
        block for_block;
    };

    struct _break {};

    struct _continue {};

    struct statement_ : public std::variant<simple_statement, _if, _while, _for, _break, _continue>
    {
        using base = std::variant<simple_statement, _if, _while, _for, _break, _continue>;
        using base::base;
    };

    /**
     * If it was necessary to have complex
     * typing logic this would go in the
     * type system implementation.
     * Now, only named variables are accessible
     * and the only binding necessary for
     * any compile time analysis is its name.
     */
    using variable_t = std::string;

    using variables_list = std::vector<variable_t>;

    struct declarations {
        std::vector<variables_list> list;
    };

    struct program {
        declarations decls;
        block statements;
    };

}

#endif //LANG_AST_HPP
