#ifndef LANG_3AC_PRINTER_HPP
#define LANG_3AC_PRINTER_HPP

#include "3ac_instruction.hpp"

namespace tac {
    std::string to_string(const instruction &i);
    std::string to_string(const static_allocation &s);
}

namespace log {
    inline
    void log(const tac::instruction &i) {
        if(tac::isa<tac::label>(i)) {
            log(tac::to_string(i));
            return;
        }
        log("\t\t" + tac::to_string(i));
    }

    inline
    void log(const tac::static_allocation &s) {
        log(tac::to_string(s));
    }

    inline
    void log(const tac::program &p) {
        for(auto &a : p.static_alloc) {
            log(a);
        }
        for(auto &i : p.code) {
            log(i);
        }
    }
}
#endif //LANG_3AC_PRINTER_HPP
