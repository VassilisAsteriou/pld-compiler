#ifndef LANG_LOGGER_HPP
#define LANG_LOGGER_HPP

#include <string>
#include <iostream>

#include "utility.hpp"

namespace log {

    inline
    void out(const std::string &message) {
        std::clog << message << std::endl;
    }

    inline
    void log(const std::string &message) {
        out("lang: " + message);
    }

    inline
    void warn(const std::string &message) {
        log("Warning: " + message);
    }

    inline
    void error(const std::string &message) {
        log("Error: " + message);
        throw compilation_error();
    }

}

#endif //LANG_LOGGER_HPP
