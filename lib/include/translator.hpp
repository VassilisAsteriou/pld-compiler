#ifndef LANG_AST_TO_TAC_HPP
#define LANG_AST_TO_TAC_HPP

#include <unordered_set>
#include <list>
#include <stack>

#include "ast.hpp"
#include "3ac_instruction.hpp"

namespace node {

    class translator {

            std::unordered_set<variable_t> symbol_table;
            tac::program rv;

            static int index;

            // Support for breaking and continuing from
            // nested loops
            using continue_target = tac::code_location;
            using break_target = tac::code_location;
            using loop_frame = std::pair<break_target, continue_target>;
            std::stack<loop_frame> loop_stack;

            void declare(const node::variable_t &);
            tac::variable_location getVar(node::variable_t var);
            tac::variable_location newTemporary();
            tac::variable_location newNonTemporary();
            tac::code_location newLabel();
            void emit(tac::instruction &&);
            void prepend(tac::instruction &&);

            template <class...Args>
            void state(const std::variant<Args...> &V);

            void state(const statement_ &);
            void state(const _if &);
            void state(const _while &);
            void state(const _for &);
            void state(const _break &);
            void state(const _continue &);
            void state(const assignment &);
            void state(const print &);
            void state(const block &);

            tac::variable_location eval(const expression_&);
            tac::variable_location eval(const binexp&);
            tac::variable_location eval(const unexp&);
            tac::variable_location eval(const int_literal&);
            tac::variable_location eval(const rvalue&);

        public:

            tac::program operator() (const program &p) &&;
    };

}

#endif //LANG_AST_TO_TAC_HPP
