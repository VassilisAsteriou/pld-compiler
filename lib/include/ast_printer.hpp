#ifndef LANG_AST_PRINTER_HPP
#define LANG_AST_PRINTER_HPP

#include "ast.hpp"

namespace log {

    void log(node::program const &p);

}

#endif //LANG_AST_PRINTER_HPP
