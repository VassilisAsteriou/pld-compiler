#ifndef LANG_UTILITY_HPP
#define LANG_UTILITY_HPP

#include <variant>
#include <functional>

class custom_exception : public std::exception {};

class no_input_file: public custom_exception {
public:
    const char *what() const noexcept override { return "Input file does not exist!"; }
};

class parsing_error : public custom_exception {
public:
    const char *what() const noexcept override { return "A parsing error has occurred!"; }
};

class compilation_error: public custom_exception {
public:
    const char *what() const noexcept override { return "An error occurred during compilation."; }
};

namespace utility {
    template<class... Ts> struct visitor : Ts... { using Ts::operator()...; };
    template<class... Ts> visitor(Ts...) -> visitor<Ts...>;
}

#endif //LANG_UTILITY_HPP
