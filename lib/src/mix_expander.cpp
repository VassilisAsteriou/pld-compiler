#include <set>
#include <utility.hpp>
#include <mix_expander.hpp>
#include <map>

#include "mix_expander.hpp"

namespace arch {
    namespace mix {

        const std::string expander::tab = "\t";

        expander::expander(std::ofstream &&out) :
                out{std::move(out)} {}

        expander::expander(const std::string &out_file_name) :
                out{out_file_name} {}

        void expander::operator()(tac::program &p) {
            // 1. Process static data allocations
            out << tab << "ORIG" << tab << "0" << std::endl;
            for(int i = 0; i < 16; ++i) {
                out << tab << "ALF" << tab << "\"     \"" << std::endl;
            }
            for(auto &a : p.static_alloc) {
                out << a.loc << tab << "CON" << tab << "0" << std::endl;
            }

            // 2. Preprocess labels
            preprocess_labels(p);

            // 3. Process instructions
            out << tab << "ORIG" << tab << "1000" << std::endl;
            for(auto &i : p.code) {
                expand(i);
            }
            out << tab << "HLT" << std::endl;

            print_overflow_exception();

            out << tab << "END" << tab << "1000" << std::endl;
        }

        template <class ...Args>
        void expander::expand(const std::variant<Args...> &v) {
            std::visit([&](const auto &vv) { return expand(vv); },
                    v);
        }

        void expander::LDA(const tac::data_operand &o) {
            std::visit(utility::visitor{
                [&] (const tac::immediate &i) {
                    if(std::abs(i) > MAX_ADDRESS) {
                        out << tab << "LDA" << tab << "=" << i << "=" << std::endl;
                    }
                    else {
                        out << tab << "ENTA" << tab << i << std::endl;
                    }
                },
                [&] (const tac::variable_location &v) {
                    out << tab << "LDA" << tab << v << std::endl;
                }
            }, o);
        }

        void expander::LDX(const tac::data_operand &o) {
            std::visit(utility::visitor{
                    [&] (const tac::immediate &i) {
                        if(std::abs(i) > MAX_ADDRESS) {
                            out << tab << "LDX" << tab << "=" << i << "=" << std::endl;
                        }
                        else {
                            out << tab << "ENTX" << tab << i << std::endl;
                        }
                    },
                    [&] (const tac::variable_location &v) {
                        out << tab << "LDX" << tab << v << std::endl;
                    }
            }, o);
        }

        void expander::STA(const tac::variable_location &v) {
            out << tab << "STA" << tab << v << std::endl;
        }

        void expander::STX(const tac::variable_location &v) {
            out << tab << "STX" << tab << v << std::endl;
        }

        void expander::LDAN(const tac::data_operand &o) {
            std::visit(utility::visitor{
                    [&] (const tac::immediate &i) {
                        if(std::abs(i) > MAX_ADDRESS) {
                            out << tab << "LDAN" << tab << "=" << i << "=" << std::endl;
                        }
                        else {
                            out << tab << "ENNA" << tab << i << std::endl;
                        }
                    },
                    [&] (const tac::variable_location &v) {
                        out << tab << "LDAN" << tab << v << std::endl;
                    }
            }, o);
        }

        void expander::ADD(const tac::data_operand &o) {
            std::visit(utility::visitor{
                    [&] (const tac::immediate &i) {
                        if(std::abs(i) > MAX_ADDRESS) {
                            out << tab << "ADD" << tab << "=" << i << "=" << std::endl;
                        }
                        else {
                            out << tab << "INCA" << tab << i << std::endl;
                        }
                    },
                    [&] (const tac::variable_location &v) {
                        out << tab << "ADD" << tab << v << std::endl;
                    }
            }, o);
        }

        void expander::SUB(const tac::data_operand &o) {
            std::visit(utility::visitor{
                    [&] (const tac::immediate &i) {
                        if(std::abs(i) > MAX_ADDRESS) {
                            out << tab << "SUB" << tab << "=" << i << "=" << std::endl;
                        }
                        else {
                            out << tab << "DECA" << tab << i << std::endl;
                        }
                    },
                    [&] (const tac::variable_location &v) {
                        out << tab << "SUB" << tab << v << std::endl;
                    }
            }, o);
        }

        void expander::MUL(const tac::data_operand &o) {
            std::visit(utility::visitor{
                    [&] (const tac::immediate &imm) {
                        out << tab << "MUL" << tab << "=" << imm << "=" << std::endl;
                    },
                    [&] (const tac::variable_location &v) {
                        out << tab << "MUL" << tab << v << std::endl;
                    }
            }, o);
        }

        void expander::DIV(const tac::data_operand &o) {
            std::visit(utility::visitor{
                    [&] (const tac::immediate &imm) {
                        out << tab << "DIV" << tab << "=" << imm << "=" << std::endl;
                    },
                    [&] (const tac::variable_location &v) {
                        out << tab << "DIV" << tab << v << std::endl;
                    }
            }, o);
            out << tab << "JOV" << tab << "OVH" << std::endl;
        }

        void expander::CMPX(const tac::data_operand &o) {
            std::visit(utility::visitor{
                    [&] (const tac::immediate &imm) {
                        out << tab << "CMPX" << tab << "=" << imm << "=" << std::endl;
                    },
                    [&] (const tac::variable_location &v) {
                        out << tab << "CMPX" << tab << v << std::endl;
                    }
            }, o);
        }

        void expander::print_overflow_exception() {
            out << "OVH" << tab << "OUT" << tab << "OVM(19)" << std::endl;
            out << tab << "HLT" << std::endl;
            print_message("OVM", "An overflow exception has occurred.");
        }

        void expander::print_message(std::string label, std::string msg) {
            out << label;
            for(size_t i = 0; i < msg.length(); i+=5) {
                out << tab << "ALF" << tab << "\"" << msg.substr(i, 5) << "\"" << std::endl;
            }
        }

        void expander::preprocess_labels(tac::program &in) {
            ///
            /// This section is copy-paste from the optimizer
            ///
            /*
             * Label cluster merger
             */
            if (in.code.size() >= 2) {
                auto &c = in.code;

                std::map<tac::code_location, tac::code_location> S;

                auto next = c.begin();
                auto instr = next++;

                /*
                 * Find label pools and keep only the primary.
                 * Point the rest to the primary
                 */
                while (next != c.end()) {
                    if (tac::isa<tac::label>(*instr) && tac::isa<tac::label>(*next)) {
                        auto &src = tac::get<tac::label>(*next)->loc;
                        auto &dest = tac::get<tac::label>(*instr)->loc;
                        S[src] = dest;
                        next = c.erase(next);
                    } else {
                        instr = next;
                        next++;
                    }
                }

                /*
                 * Swap all references to non-primary labels to
                 * the corresponding primary.
                 */
                for (auto &i : c) {
                    if (auto x = tac::get<tac::unconditional_jump>(i)) {
                        if (S.find(x->target) != S.end()) {
                            i = tac::unconditional_jump{S[x->target]};
                        }
                    } else if (auto y = tac::get<tac::if_zero_jump>(i)) {
                        if (S.find(y->target) != S.end()) {
                            i = tac::if_zero_jump{S[y->target], y->value};
                        }
                    } else if (auto z = tac::get<tac::if_nonzero_jump>(i)) {
                        if (S.find(z->target) != S.end()) {
                            i = tac::if_nonzero_jump{S[z->target], z->value};
                        }
                    }
                }
            }
        }

        void expander::expand(const tac::instruction &i) {
            std::visit([&](const auto &ii) { return expand(ii); },
                    (tac::instruction::base &) i);
        }

        void expander::expand(const tac::label &l) {
            out << l.loc;
        }

        void expander::expand(const tac::direct_assignment &a) {
            LDA(a.src);
            STA(a.dest);
        }

        void expander::expand(const tac::unop_assignment &ua) {
            switch (ua.op) {

                case tac::neg:
                    LDAN(ua.src);
                    STA(ua.dest);
                    break;
                case tac::lnot:
                    LDA(0);
                    LDX(ua.src);
                    out << tab << "JXNZ" << tab << "1N" << std::endl;
                    LDA(1);
                    out << "1H"; STA(ua.dest);
                    break;
            }
        }

        void expander::expand(const tac::binop_assignment &a) {
            auto ba = a;
            ba.normalize();
            switch (ba.op) {

                case tac::add:
                    LDA(ba.src1);
                    ADD(ba.src2);
                    STA(ba.dest);
                    break;
                case tac::sub:
                    LDA(ba.src1);
                    SUB(ba.src2);
                    STA(ba.dest);
                    break;
                case tac::mul:
                    LDA(ba.src1);
                    MUL(ba.src2);
                    STX(ba.dest);
                    break;
                case tac::div:
                    LDA(ba.src1);
                    out << tab << "SRAX" << tab << "5" << std::endl;
                    DIV(ba.src2);
                    STA(ba.dest);
                    break;
                case tac::mod:
                    LDA(ba.src1);
                    out << tab << "SRAX" << tab << "5" << std::endl;
                    DIV(ba.src2);
                    STX(ba.dest);
                    break;
                case tac::less:
                    LDA(1);
                    LDX(ba.src1);
                    CMPX(ba.src2);
                    out << tab << "JL" << tab << "1F" << std::endl;
                    LDA(0);
                    out << "1H";
                    STA(ba.dest);
                    break;
                case tac::equal:
                    LDA(1);
                    LDX(ba.src1);
                    CMPX(ba.src2);
                    out << tab << "JE" << tab << "1F" << std::endl;
                    LDA(0);
                    out << "1H";
                    STA(ba.dest);
                    break;
                case tac::less_or_equal:
                    LDA(1);
                    LDX(ba.src1);
                    CMPX(ba.src2);
                    out << tab << "JLE" << tab << "1F" << std::endl;
                    LDA(0);
                    out << "1H";
                    STA(ba.dest);
                    break;
                case tac::not_equal:
                    LDA(1);
                    LDX(ba.src1);
                    CMPX(ba.src2);
                    out << tab << "JNE" << tab << "1F" << std::endl;
                    LDA(0);
                    out << "1H";
                    STA(ba.dest);
                    break;
            }
        }

        void expander::expand(const tac::unconditional_jump &uj) {
            out << tab << "JSJ" << tab << uj.target << std::endl;
        }

        void expander::expand(const tac::if_zero_jump &zj) {
            LDA(zj.value);
            out << tab << "JAZ" << tab << zj.target << std::endl;
        }

        void expander::expand(const tac::if_nonzero_jump &nzj) {
            LDA(nzj.value);
            out << tab << "JANZ" << tab << nzj.target << std::endl;
        }

        void expander::expand(const tac::nop &) {
            out << tab << "NOP" << std::endl;
        }

        void expander::expand(const tac::print &p) {
            LDA(p.op);
            out << tab << "JANN" << tab << "1F" << std::endl;
            out << tab << "ENTX" << tab << "45" << std::endl;
            STX({"0"});
            out << "1H" << tab << "CHAR" << std::endl;
            STA({"1"});
            STX({"2"});
            out << tab << "OUT" << tab << "0(19)" << std::endl;
            out << tab << "STZ" << tab << "0" << std::endl;
        }
    }
}
