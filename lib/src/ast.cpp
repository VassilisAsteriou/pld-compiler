#include "ast.hpp"

namespace node {

    std::string to_string(operator_t opt) {
        std::string op;
        switch (opt) {

            case node::none:           op = ""; break;
            case node::add:            op = "+"; break;
            case node::sub:            op = "-"; break;
            case node::mul:            op = "*"; break;
            case node::div:            op = "/"; break;
            case node::mod:            op = "%"; break;
            case node::neg:            op = "-"; break;
            case node::assign:         op = "="; break;
            case node::add_assign:     op = "+="; break;
            case node::sub_assign:     op = "-="; break;
            case node::mul_assign:     op = "*="; break;
            case node::div_assign:     op = "/="; break;
            case node::mod_assign:     op = "%="; break;
            case node::less:           op = "<"; break;
            case node::greater:        op = ">"; break;
            case node::less_equal:     op = "<="; break;
            case node::greater_equal:  op = ">="; break;
            case node::equal:          op = "=="; break;
            case node::not_equal:      op = "!="; break;
            case node::logic_and:      op = "&&"; break;
            case node::logic_or:       op = "||"; break;
            case node::logic_not:      op = "!"; break;
        }
        return op;
    }

}