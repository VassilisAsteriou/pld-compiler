#include "3ac_printer.hpp"
#include "utility.hpp"

namespace tac {

    std::string to_string(const string_location &l) {
        return l;
    }

    std::string to_string(const immediate &i) {
        return std::to_string(i);
    }

    std::string to_string(const nop &) {
        return "nop";
    }

    std::string to_string(unop op) {
        switch (op) {

            case neg:
                return "-";
            case lnot:
                return "!";
        }
        return "";
    }

    std::string to_string (binop op) {
        switch (op) {
            case add:
                return "+";
            case sub:
                return "-";
            case mul:
                return "*";
            case div:
                return "/";
            case mod:
                return "%";
            case less:
                return "<";
            case equal:
                return "==";
            case less_or_equal:
                return "<=";
            case not_equal:
                return "!=";
        }
        return "";
    }

    // For all variants

    template <class... Args>
    std::string to_string(const std::variant<Args...> &v) {
        return std::visit([] (const auto &v) { return to_string(v); },
                          v);
    }

    std::string to_string(const direct_assignment &d) {
        return to_string(d.dest) + " = " + to_string(d.src);
    }

    std::string to_string(const unop_assignment &u) {
        return to_string(u.dest) + " = " + to_string(u.op)
               + to_string(u.src);
    }

    std::string to_string(const binop_assignment &b) {
        return to_string(b.dest) + " = "
               + to_string(b.src1)
               + " " + to_string(b.op) + " "
               + to_string(b.src2);
    }

    std::string to_string(const unconditional_jump &b) {
        return "goto " + to_string(b.target);
    }

    std::string to_string(const if_nonzero_jump &b) {
        return "ifnZ " + to_string(b.value)
               + " goto " + to_string(b.target);
    }

    std::string to_string(const if_zero_jump &b) {
        return "ifZ " + to_string(b.value)
               + " goto " + to_string(b.target);
    }

    std::string to_string(const label &l) {
        return to_string(l.loc) + ":";
    }

    std::string to_string(const print &p) {
        return "print " + to_string(p.op);
    }

    std::string to_string(const instruction &i) {
        return std::visit([&] (const auto &i) { return to_string(i); },
                          (instruction::base &)i);
    }

    std::string to_string(const static_allocation &s) {
        return s.loc;
    }
}