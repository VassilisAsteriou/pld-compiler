
#include <3ac_instruction.hpp>

#include "utility.hpp"
#include "3ac_instruction.hpp"

namespace tac {

    instruction::operator base const &() const {
        return *this;
    }

    template <class C>
    inline
    bool _isConstexpr(C) {
        return false;
    }

    inline
    bool _isConstexpr(const unop_assignment &ua) {
        return std::holds_alternative<immediate>(ua.src);
    }

    inline
    bool _isConstexpr(const binop_assignment &ba) {
        return std::holds_alternative<immediate>(ba.src1)
                && std::holds_alternative<immediate>(ba.src2);
    }

    inline
    bool _isConstexpr(const if_zero_jump &j) {
        return std::holds_alternative<immediate>(j.value);
    }

    inline
    bool _isConstexpr(const if_nonzero_jump &j) {
        return std::holds_alternative<immediate>(j.value);
    }

    template <class ...Args>
    bool _isConstexpr(const std::variant<Args...> &V) {
        return std::visit(utility::visitor{
            [](const auto &v) { return _isConstexpr(v); }
            }, V);
    }

    bool instruction::isConstexpr() {
        return std::visit(utility::visitor{
            [](const auto &c) { return _isConstexpr(c); }
        }, (instruction::base &)*this);
    }

    std::set<variable_location> instruction::use() const {
        return std::visit([&](const auto &j) -> std::set<variable_location> {
            return std::visit(utility::visitor{
                [&](const direct_assignment &d) -> std::set<variable_location> {
                    if(std::holds_alternative<variable_location>(d.src)) {
                        return { std::get<variable_location>(d.src) };
                    }
                    return { };
                },
                [&](const unop_assignment &u) -> std::set<variable_location> {
                    if(std::holds_alternative<variable_location>(u.src)) {
                        return {std::get<variable_location>(u.src)};
                    }
                    return { };
                },
                [&](const binop_assignment &b) -> std::set<variable_location> {
                    std::set<variable_location> S;
                    if(std::holds_alternative<variable_location>(b.src1)) {
                        S.insert(std::get<variable_location>(b.src1));
                    }
                    if(std::holds_alternative<variable_location>(b.src2)) {
                        S.insert(std::get<variable_location>(b.src2));
                    }
                    return S;
                },
                [&](const if_zero_jump &zj) -> std::set<variable_location> {
                    if(std::holds_alternative<variable_location>(zj.value)) {
                        return {std::get<variable_location>(zj.value)};
                    }
                    return { };
                },
                [&](const if_nonzero_jump &nzj) -> std::set<variable_location> {
                    if(std::holds_alternative<variable_location>(nzj.value)) {
                        return {std::get<variable_location>(nzj.value)};
                    }
                    return { };
                },
                [&](const print &p) -> std::set<variable_location> {
                    if(std::holds_alternative<variable_location>(p.op)) {
                        return {std::get<variable_location>(p.op)};
                    }
                    return { };
                },
                [&](const auto &) -> std::set<variable_location> {
                    return { };
                }
            }, j);
        }, (base) *this);
    }


    std::optional<variable_location> instruction::def() const {
        using rt = std::optional<variable_location>;
        return std::visit(utility::visitor{
            [&](const assignment &a) -> rt {
                return std::visit(
                    [&](const auto &aa) -> rt {
                        return aa.dest;
                    }, a);
            },
            [&](const auto &other) ->rt {
                return std::nullopt;
            }
            }, (base) *this);
    }

    bool instruction::dispensible() const {
        return !(isa<print>(*this) || std::holds_alternative<jump>((base&)*this));
    }

    void binop_assignment::normalize() {
        switch (op) {

            case add:       /* fall through */
            case mul:       /* fall through */
            case equal:     /* fall through */
            case not_equal:
                if(std::holds_alternative<variable_location>(src1)
                   && std::holds_alternative<immediate>(src2)) {
                    std::swap(src1, src2);
                }
            default:
                break;
        }

    }
}