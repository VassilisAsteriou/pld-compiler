#include <utility>
#include <variant>

#include "utility.hpp"
#include "ast_printer.hpp"
#include "logger.hpp"

namespace log {

    static const std::string increment = "    ";

    void log(node::expression_ const &, std::string const &);
    void log(node::rvalue const &, std::string);
    void log(node::int_literal const &, std::string);
    void log(node::binexp const &, std::string);
    void log(node::unexp const &, std::string);
    void log(node::lvalue const &, std::string);
    void log(node::statement_ const &, std::string const &);
    void log(node::block const &, std::string);
    void log(node::assignment const &, std::string);
    void log(node::print const &, std::string);
    void log(node::simple_statement const &, std::string const &);
    void log(node::_if const &, std::string);
    void log(node::_while const &, std::string);
    void log(node::_for const &, std::string);
    void log(node::_break const &, std::string const &);
    void log(node::_continue const &, std::string const &);
    void log(node::declarations const &, std::string);
    void log(node::program const &);

    void log(node::program const &p) {
        log("Program");
        std::string pref = increment;
        log(p.decls, pref);
        log(p.statements, pref);
    }

    void log(const node::declarations &d, std::string prefix) {
        log(prefix + "Variable Declarations");
        prefix += increment + " - ";
        for (auto const &l: d.list) {
            for (auto const &v: l) {
                log(prefix + v);
            }
            log("");
        }
    }

    void log(const node::statement_ &s, const std::string &prefix) {
        std::visit(utility::visitor{
                [&](const auto &s) { log(s, prefix); }
        }, (node::statement_::base &) s);
    }

    void log(const node::simple_statement &statement, const std::string &prefix) {
        std::visit(utility::visitor{
                [&](const auto &as) { log(as, prefix); }
        }, statement);
    }

    void log(const node::assignment &assignment, std::string prefix) {
        log(prefix + "Assignment");
        prefix += increment;
        log(assignment.var, prefix);
        log(*assignment.expr, prefix);
    }

    void log(const node::print &print, std::string prefix) {
        log(prefix + "Print");
        prefix += increment;
        log(*print.expr, prefix);
    }

    void log(const node::_if &stmt, std::string prefix) {
        log(prefix + "If clause");
        prefix += increment;
        log(*stmt.condition, prefix);
        log(stmt.if_block, prefix);
        log(stmt.else_block, prefix);
    }

    void log(const node::_while &stmt, std::string prefix) {
        log(prefix + "While clause");
        prefix += increment;
        log(*stmt.condition, prefix);
        log(stmt.while_block, prefix);
    }

    void log(const node::_for &stmt, std::string prefix) {
        log(prefix + "For clause");
        prefix += increment;
        log(stmt.init, prefix + "init: ");
        log(*stmt.condition, prefix + "cond: ");
        log(stmt.update, prefix + "updt: ");
        log(stmt.for_block, prefix);
    }

    void log(const node::_continue &stmt, const std::string &prefix) {
        log(prefix + "Continue");
    }

    void log(const node::_break &stmt, const std::string &prefix) {
        log(prefix + "Break");
    }

    void log(const node::block &s, std::string prefix) {
        log(prefix + "Statement List");
        prefix += increment;
        for (auto &st : s) {
            log(*st, prefix);
        }
    }

    void log(node::lvalue const &lvalue, std::string prefix) {
        log(prefix += "Lvalue: " + lvalue.name);
    }

    void log(const node::expression_ &exp, const std::string &prefix) {
        std::visit(utility::visitor{
                [&](const auto &exp) { log(exp, prefix); }
        }, (node::expression_::base &) exp);
    }

    void log(const node::rvalue &rvalue, std::string prefix) {
        log(prefix += "Rvalue: " + rvalue.name);
    }

    void log(const node::int_literal &lit, std::string prefix) {
        log(prefix += "32bit integral literal: " + std::to_string(lit.value));
    }

    void log(const node::binexp &exp, std::string prefix) {
        log(prefix + "Binary expression: " + node::to_string(exp.op));
        prefix += increment;
        log(*exp.op1, prefix);
        log(*exp.op2, prefix);
    }

    void log(const node::unexp &exp, std::string prefix) {
        log(prefix + "Unary expression: " + node::to_string(exp.op));
        prefix += increment;
        log(*exp.op1, prefix);
    }
}