
#include <translator.hpp>

#include "translator.hpp"

#include "utility.hpp"
#include "logger.hpp"

namespace node {

    using namespace tac;

    tac::program translator::operator()(const program &p) && {

        /*
         * Phase 1: Process variable declarations
         * If declarations intertwined with statements were
         * allowed, a more general declare would be available
         * and var declarations would be processed in the
         * course of translation, leading to a non-static
         * symbol table.
         */
        for(auto &vL : p.decls.list) {
            for(auto &v : vL) {
                declare(v);
            }
        }

        /*
         * Phase 2: Process the program command by command.
         */
        for(auto &stmt : p.statements) {
            state(*stmt);
        }

        return std::move(rv);
    }

    inline
    void translator::declare(const node::variable_t &v) {
        if(!symbol_table.insert(v).second) {
            // If no actual insertion took place
            // warn about duplicate declaration
            log::error("Duplicate declaration of " + v);
        } else {
            rv.static_alloc.push_back(static_allocation{v});
        }
    }

    tac::variable_location translator::getVar(node::variable_t var) {
        auto it = symbol_table.find(var);
        if(it == symbol_table.end()) {
            log::error("Usage of undeclared variable " + var);
            return variable_location{"__Undefined_Location__"};
        }
        return variable_location{var};

    }

    tac::variable_location translator::newTemporary() {
        static unsigned int tmp_counter = 0;
        std::string name;
        do {
            name = "t" + std::to_string(tmp_counter++);
        } while(symbol_table.find(name) != symbol_table.end());
        variable_location l{name};
        rv.static_alloc.push_back(static_allocation{l});
        return l;

    }

    tac::code_location translator::newLabel() {
        static unsigned int label_counter = 0;
        return code_location{"L" + std::to_string(label_counter++)};

    }

    void translator::emit(tac::instruction &&i) {
        rv.code.emplace_back(i);
    }

    inline
    void translator::state(const _if &S) {
        auto L_else = newLabel();
        auto L_fi   = newLabel();

        auto cond = eval(*S.condition);
        emit(if_zero_jump{L_else, cond});
        state(S.if_block);
        emit(unconditional_jump{L_fi});
        emit(label{L_else});
        state(S.else_block);
        emit(label{L_fi});
    }

    inline
    void translator::state(const _while &S) {
        auto L_loop = newLabel();
        auto L_endloop = newLabel();

        emit(label{L_loop});
        auto cond = eval(*S.condition);
        emit(if_zero_jump{L_endloop, cond});

        loop_stack.push({L_endloop, L_loop});
        state(S.while_block);
        loop_stack.pop();

        emit(unconditional_jump{L_loop});
        emit(label{L_endloop});
    }

    inline
    void translator::state(const _for &S) {
        auto L_loop = newLabel();
        auto L_continue = newLabel();
        auto L_endloop = newLabel();

        state(S.init);
        emit(label{L_loop});
        auto cond = eval(*S.condition);
        emit(if_zero_jump{L_endloop, cond});

        loop_stack.push({L_endloop, L_continue});
        state(S.for_block);
        loop_stack.pop();

        emit(label{L_continue});
        state(S.update);
        emit(unconditional_jump{L_loop});
        emit(label{L_endloop});
    }

    inline
    void translator::state(const _break &) {
        emit(unconditional_jump{loop_stack.top().first});
    }

    inline
    void translator::state(const _continue &) {
        emit(unconditional_jump{loop_stack.top().second});
    }

    inline
    void translator::state(const assignment &S) {
        auto expr = eval(*S.expr);
        auto dest = getVar(S.var.name);
        switch (S.op) { // TODO split operators to different enums

            case assign:
                emit(direct_assignment{dest, expr});
                break;
            case add_assign:
                emit(binop_assignment{dest, dest, expr, tac::add});
                break;
            case sub_assign:
                emit(binop_assignment{dest, dest, expr, tac::sub});
                break;
            case mul_assign:
                emit(binop_assignment{dest, dest, expr, tac::mul});
                break;
            case div_assign:
                emit(binop_assignment{dest, dest, expr, tac::div});
                break;
            case mod_assign:
                emit(binop_assignment{dest, dest, expr, tac::mod});
                break;
        }
    }

    inline
    void translator::state(const print &S) {
        auto expr = eval(*S.expr);
        emit(tac::print{expr});
    }

    inline
    void translator::state(const block &B) {
        for(auto &s : B) {
            state(*s);
        }
    }

    template <class...Args>
    inline
    void translator::state(const std::variant<Args...> &V) {
        std::visit(utility::visitor{
                [&](const auto &statement) { return state(statement); }
        }, V);
    }

    inline
    void translator::state(const statement_ &s) {
        state((statement_::base &)s);
    }

    tac::variable_location translator::eval(const expression_ &E) {
        return std::visit(utility::visitor{
            [&] (const auto &expr) { return eval(expr); }
        }, (expression_::base &) E);
    }

    tac::variable_location translator::eval(const binexp &E) {
        auto t = newTemporary();

        if(E.op == logic_and) {
            auto L = newLabel();

            emit(direct_assignment{t, 0});
            auto e1 = eval(*E.op1);
            emit(if_zero_jump{L, e1});
            auto e2 = eval(*E.op2);
            emit(direct_assignment{t, e2});
            emit(label{L});
        }
        else if (E.op == logic_or) {
            auto L = newLabel();

            emit(direct_assignment{t, 1});
            auto e1 = eval(*E.op1);
            emit(if_nonzero_jump{L, e1});
            auto e2 = eval(*E.op2);
            emit(direct_assignment{t, e2});
            emit(label{L});
        }
        else {

            auto e1 = eval(*E.op1);
            auto e2 = eval(*E.op2);

            switch (E.op) {

                case add:
                    emit(binop_assignment{t, e1, e2, tac::add});
                    break;
                case sub:
                    emit(binop_assignment{t, e1, e2, tac::sub});
                    break;
                case mul:
                    emit(binop_assignment{t, e1, e2, tac::mul});
                    break;
                case div:
                    emit(binop_assignment{t, e1, e2, tac::div});
                    break;
                case mod:
                    emit(binop_assignment{t, e1, e2, tac::mod});
                    break;

                case less:
                    emit(binop_assignment{t, e1, e2, tac::less});
                    break;
                case greater:
                    emit(binop_assignment{t, e2, e1, tac::less});
                    break;
                case less_equal:
                    emit(binop_assignment{t, e1, e2, tac::less_or_equal});
                    break;
                case greater_equal:
                    emit(binop_assignment{t, e2, e1, tac::less_or_equal});
                    break;
                case equal:
                    emit(binop_assignment{t, e1, e2, tac::equal});
                    break;
                case not_equal:
                    emit(binop_assignment{t, e1, e2, tac::not_equal});
                    break;
            }
        }

        return t;
    }

    tac::variable_location translator::eval(const unexp &E) {
        auto t = newTemporary();
        auto e = eval(*E.op1);
        switch (E.op) {
            case neg:
                emit(unop_assignment{t, e, tac::neg});
                break;
            case logic_not:
                emit(unop_assignment{t, e, tac::lnot});
                break;
        }
        return t;
    }

    tac::variable_location translator::eval(const int_literal &i) {
        auto t = newTemporary();
        emit(direct_assignment{t, i.value});
        return t;
    }

    tac::variable_location translator::eval(const rvalue &rv) {
        return getVar(rv.name);
    }

}