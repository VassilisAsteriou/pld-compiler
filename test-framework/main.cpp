#include <iostream>
#include "UnitTest.hpp"

int main() {
    return Test::UnitTest::run() ? 0 : -1;
}
