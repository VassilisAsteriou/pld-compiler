#include "UnitTest.hpp"

#include <vector>

using std::vector;

#include <iostream>

using std::cout;
using std::cout;
using std::endl;

#include <string>

using std::string;

#include <cmath>

using std::abs;

namespace Test {

    // static variable initialization
    vector<UnitTest *> *UnitTest::listOfTests = nullptr;

    bool UnitTest::run(void)
    {
        if (listOfTests == nullptr) {
            cout << "No tests to run" << endl;
            return true;
        }
        else if (listOfTests->size() == 0) {
            cout << "No tests to run" << endl;
            return true;
        }
        cout << "~~     Running all tests    ~~" << endl;

        unsigned int successfulTests = 0;
        for(UnitTest *unitTest: *listOfTests) {
            if (unitTest->runTest()) successfulTests++;
            delete unitTest;
        }

        cout << "~~   Testing has finished   ~~" << endl;
        cout << successfulTests << " out of " << listOfTests->size() <<
             " tests were run successfully." << endl;

        if (successfulTests == listOfTests->size()) {
            cout << "SUCCESS" << endl;
            return true;
        }
        else {
            cout << "FAILURE" << endl;
            return false;
        }

        delete listOfTests;
    }

    bool UnitTest::runTest()
    {
        (*this)();
        return success;
    }

    void UnitTest::Assert(const bool &expression, const string &failString)
    {
        if(!expression) {
            success = false;
            cout << "Expression assertation failed: " << failString << endl;
        }
    }

    // Prohibit double initialization; use tolerance based instead
    template <>
    void UnitTest::assertEquals<double>(const double &expected, const double &actual, const string&);
//    {
//        cout << "Warning: Assert equals called on double. Failing test.";
//        success = false;
//    }

    void UnitTest::assertEquals(
            const double &expected,
            const double &actual,
            const double &tolerance,
            const string fail)
    {
        if( abs(expected - actual) > tolerance) {
            success = false;
            if(fail == "") {
                std::cout << "Equality test failed: expected " << expected << " but got "
                          << actual << " instead." << std::endl;
            }
            else {
                std::cout << "Equality test failed: " << fail << std::endl;
            }
        }
    }

}