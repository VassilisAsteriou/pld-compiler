#ifndef _AI_TESTING_FRAMEWORK_UNIT_TEST_H
#define _AI_TESTING_FRAMEWORK_UNIT_TEST_H

/**
 * This tool is NOT: a fully-fledged unit testing framework.
 * Instead it is a small utility in order to organize and
 * semi-automate your tests. It works by YOU registering
 * all unit test classes to the utility and YOU initiating
 * the call to the tests.
 *
 * How to use this basic unit testing utility:
 *
 * Say you write a class A, which you want to test.
 *
 *  1. Create a class ATest in a parallel test directory.
 *  2. Include and subclass Test::UnitTest.
 *  3. Override the "virtual void operator() (void)" function.
 *  4. Create your specific tests as individual member functions
 *      of your class ATest, for a modular result.
 *  5. In each test use the assertation functions to test your code.
 *      They are basically wrappers to basic utilities for counting
 *      failures and similar minute functionality you could've
 *      written your self.
 *  6. Don't forget to CALL all your test member functions from
 *      operator().
 *  7. Register a testing class object with the utility: declare a
 *      static boolean variable and assign it the result of
 *      Test::UnitTest::registerUnitTest(new ATest(...)). Do not
 *      forget to extern it in the header, so that the compiler
 *      actually issues a run of the registration as part of
 *      the static variable initialization phase of runtime.
 *  8. To run your tests just call Test::UnitTest::run() from main.
 *  9. The results do not count specific tests (i.e. methods in
 *      the class), but rather entire suites (i.e. test classes)
 *      A suite can either pass or fail as a whole.
 */

    // TODO Have the user register the tests instead of just calling them.
    // TODO Count assertations per suite, and per test. Make fail/pass more granular.
    // TODO Test whether multiple tests work.

#include <vector>
#include <string>
#include <iostream>
#include <functional>

namespace Test {

    class UnitTest {

        // Framework setup

        private:

            static std::vector<UnitTest *> *listOfTests;

        public:

            /**
             * Register your class with the utility. You need to do this
             * in order to ensure that your tests will be queued for running
             * once run() is invoked.
             *
             * Example usage:
             *
             * // myTestClass.cpp
             * // static variable definition
             * bool var = UnitTest::registerUnitTest(new myTestClass());
             *
             * @param unitTest A pointer to a unit test. After running,
             *      delete is called on this parameter.
             */
            inline static bool registerUnitTest(UnitTest *unitTest) noexcept
            {
                if(listOfTests == nullptr) {
                    listOfTests = new std::vector<UnitTest *>();
                }
                listOfTests->push_back(unitTest); return true;
            }


            /**
             * Testing entry point. Call this function from main.
             * As a rule of thumb you should call this function only once, since
             * it deletes unit tests passed as pointers!!
             */
            static bool run(void);

            /**
             * This is the function called by the framework. So this
             * is the function a subclass should override.
             */
            virtual void operator() (void) = 0;

        // Subclass functionality

        private:

            bool runTest(void);

        protected:

            // Prohibit instantiation of abstract UnitTest

            virtual ~UnitTest() = default;

            /**
             * Variable representing the status of the unit test.
             * Should you write an assertation function on your own,
             * make sure you modify this variable to manifest failed
             * assertations.
             */
            bool success = true;

            /**
             * General assertation function.
             * @param expression A boolean expression representing the assertation.
             * @param failString A string to be printed in the event of failure.
             */
            void Assert(
                    const bool &expression,
                    const std::string &failString = "Assertation failed!");

            /**
             * Generic assertEquals function.
             * On error it prints on cout.
             * You should ONLY call this one if the objects you are checking for
             * equality can be printed on cout (override operator <<).
             * @tparam T Type of objects compared.
             * @param expected Expected value of computation.
             * @param actual Computed value.
             */
            template<typename T>
            void assertEquals(
                    const T &expected,
                    const T &actual,
                    const std::string &fail = "")
            {
                if(!(expected == actual)) {
                    success = false;
                    if(fail == "") {
                        std::cout << "Equality test failed" /*
                                 ": expected " << expected << " but got "
                                  << actual << " instead." //*/
                                  << std::endl;
                    }
                    else {
                        std::cout << "Equality test failed: " << fail << std::endl;
                    }
                }
            }

            /**
             * Version of assertEquals tuned for double values.
             * @param expected Expected value of computation.
             * @param actual Actual computed value.
             * @param tolerance Maximal error used in the comparison.
             */
            void assertEquals(
                    const double &expected,
                    const double &actual,
                    const double &tolerance,
                    std::string fail = "");

            template <class Exception>
            void assertThrows(const std::function<void(void)> &f, const std::string &fail = "Exception expected")
            {
                try {
                    f();
                    Assert(false, fail);
                }
                catch (Exception &) {
                    Assert(true);
                }
                catch (...) {
                    Assert(false, "Wrong exception!");
                }
            }

            template <class Exception>
            void assertNoThrow(const std::function<void(void)> &f, const std::string &fail = "Exception thrown!")
            {
                try {
                    f();
                    Assert(true);
                }
                catch (Exception &) {
                    Assert(false, fail);
                }
                catch (...) {
                    Assert(false, fail + "Wrong exception tho!");
                }
            }


    };

}   // namespace Test

#endif // _AI_TESTING_FRAMEWORK_UNIT_TEST_H